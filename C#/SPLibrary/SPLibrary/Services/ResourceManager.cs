﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Utilities;
using System.Reflection;

namespace SPLibrary.Services
{
    class ResourceManager
    {
        const string FILEKEY = "FILE";

        /// <summary>
        /// Get term/string inside a resource file based on the specified entity
        /// </summary>
        /// <typeparam name="T">Desired Class</typeparam>
        /// <param name="key">Key name in your resource file</param>
        /// <returns>Localized resource string</returns>
        public static string GetResourceByKey<T>(string key)
        {
            try
            {
                return LoadResource(GetFileSource(typeof(T), FILEKEY), key);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get resource file name
        /// </summary>
        /// <param name="type"></param>
        /// <param name="constantKey"></param>
        /// <returns></returns>
        private static string GetFileSource(Type type, string constantKey)
        {
            try
            {
                string property = string.Empty;
                foreach (FieldInfo item in type.GetFields())
                {
                    if (item.IsLiteral && item.Name == constantKey)
                    {
                        property = Convert.ToString(item.GetRawConstantValue());
                        break;
                    }
                }

                return property;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Load Resource files and get the specified key value
        /// </summary>
        /// <param name="fileName">resource file name</param>
        /// <param name="key">resource key</param>
        /// <returns>localized key value</returns>
        private static string LoadResource(string fileName, string key)
        {
            try
            {
                return SPUtility.GetLocalizedString(string.Format("$Resources:{0}", key), fileName, (uint)CultureInfo.CurrentCulture.LCID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
