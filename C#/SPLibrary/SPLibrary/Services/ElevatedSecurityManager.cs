﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace SPLibrary.SecurityManager
{
    class ElevatedSecurityManager
    {
        /// <summary>
        /// Delegation of code to run with elevated privileges
        /// </summary>
        /// <param name="targetWeb">SPWeb object to run the desired code.</param>
        public delegate void CodeToElevate(SPWeb targetWeb);

        /// <summary>
        /// Method to run the elevated code
        /// </summary>
        /// <param name="code">Custom elevated code.</param>
        public void Run(CodeToElevate code)
        {
            this.Run(code, false);
        }

        /// <summary>
        /// Method to run the elevated code
        /// </summary>
        /// <param name="code">Custom elevated code.</param>
        /// <param name="validateDigest">default value = false, if validateDigest is true, validate formDigest for the currentpage</param>
        public void Run(CodeToElevate code, bool validateDigest)
        {
            this.Run(code, validateDigest, SPContext.Current.Site.Url);
        }

        /// <summary>
        /// Method to run the elevated code
        /// </summary>
        /// <param name="code">Custom elevated code.</param>
        /// <param name="validateDigest">default value = false, if validateDigest is true, validate formDigest for the currentpage</param>
        /// <param name="siteUrl">Current site Url, default value = SPContext.Current.Site.Url</param>
        public void Run(CodeToElevate code, bool validateDigest, string siteUrl)
        {
            this.Run(code, validateDigest, siteUrl, SPContext.Current.Web.ServerRelativeUrl);
        }

        /// <summary>
        /// Method to run the elevated code
        /// </summary>
        /// <param name="code">Custom elevated code.</param>
        /// <param name="validateDigest">default value = false, if validateDigest is true, validate formDigest for the currentpage</param>
        /// <param name="siteUrl">Current Site Url, default value = SPContext.Current.Site.Url</param>
        /// <param name="webUrl">Current Web Url, default value = SPContext.Current.Web.ServerRelativeUrl</param>
        public void Run(CodeToElevate code, bool validateDigest, string siteUrl, string webUrl)
        {

            SPSecurity.CodeToRunElevated elevatedCode = new SPSecurity.CodeToRunElevated(ElevatedCode(code, siteUrl, webUrl));

            if (validateDigest) SPUtility.ValidateFormDigest();

            SPSecurity.RunWithElevatedPrivileges(elevatedCode);
        }

        /// <summary>
        /// Delegate a custom code to run with elevated privileges
        /// </summary>
        /// <param name="code">Custom code to elevate</param>
        /// <param name="siteUrl">Current Site Url, default value = SPContext.Current.Site.Url</param>
        /// <param name="webUrl">Current Web Url, default value = SPContext.Current.Web.ServerRelativeUrl</param>
        /// <returns>Current custom code delegated to run with elevated privileges</returns>
        private SPSecurity.CodeToRunElevated ElevatedCode(CodeToElevate code, string siteUrl, string webUrl)
        {
            SPSecurity.CodeToRunElevated elevatedCode = new SPSecurity.CodeToRunElevated(delegate
            {
                using (SPSite target = new SPSite(siteUrl))
                {
                    using (SPWeb targetWeb = target.OpenWeb(webUrl))
                    {
                        code(targetWeb);
                    }
                }
            });
            return elevatedCode;
        }
    }
}
