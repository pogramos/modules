QUnit.test("(start && end) < first period", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 7:34:33"),
		end = new Date("03/19/2015 7:34:33"),
		expect = {days: 0, noFormat: 0, hours: 0, minutes: 0, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("start < first period && (end > first period && end < (end first period))", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 7:34:33"),
		end = new Date("03/19/2015 11:34:33"),
		expect = {days: 0, noFormat: 3, hours: 3, minutes: 35, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("(start > first period && start < (end first period)) && (end > first period && end < (end first period))", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 8:34:33"),
		end = new Date("03/19/2015 11:34:33"),
		expect = {days: 0, noFormat: 3, hours: 3, minutes: 0, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("(start > first period && start < (end first period)) && end > first period", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 8:34:33"),
		end = new Date("03/19/2015 12:34:33"),
		expect = {days: 0, noFormat: 3, hours: 3, minutes: 25, seconds: 27 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("No first period", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 12:34:33"),
		end = new Date("03/19/2015 13:34:33"),
		expect = {days: 0, noFormat: 0, hours: 0, minutes: 35, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("Full first period", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 7:34:33"),
		end = new Date("03/19/2015 13:34:33"),
		expect = {days: 0, noFormat: 4, hours: 4, minutes: 35, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("start < first period && (1day + (end > first period && end < (end first period))", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 7:34:33"),
		end = new Date("03/20/2015 11:34:33"),
		expect = {days: 0, noFormat: 10, hours: 10, minutes: 35, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("Start Date < Starting period and Ending Date > Starting2nd and < Ending2nd", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 7:34:33"),
		end = new Date("03/20/2015 15:34:33"),
		expect = {days: 0, noFormat: 13, hours: 13, minutes: 35, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 16);

	assert.deepEqual(actual, expect);
});

QUnit.test("Changing year", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("12/31/2014 7:34:33"),
		end = new Date("01/02/2015 15:34:33"),
		expect = {days: 0, noFormat: 22, hours: 22, minutes: 35, seconds : 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 17);

	assert.deepEqual(actual, expect);
});

QUnit.test("Intervalo de tempo 1", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/19/2015 16:34:33"),
		end = new Date("03/21/2015 11:07:46"),
		expect = {days: 0, noFormat: 8, hours: 8, minutes: 25, seconds: 27 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 17);

	assert.deepEqual(actual, expect);
});

QUnit.test("Intervalo de tempo 2", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/27/2015 16:28:12"),
		end = new Date("03/29/2015 08:48:33"),
		expect = {days: 0, noFormat: 0, hours: 0, minutes: 32, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 17);

	assert.deepEqual(actual, expect);
});

QUnit.test("Intervalo de tempo 3", function(assert) {
	var calculatePeriod = new CalculatePeriod(),
		start = new Date("03/13/2015 17:15:22"),
		end = new Date("03/26/2015 12:44:47"),
		expect = {days: 2, noFormat: 68, hours: 20, minutes: 0, seconds: 0 },
		actual = calculatePeriod.calculatePeriod(start, end, 8, 12, 13, 17);

	assert.deepEqual(actual, expect);
});