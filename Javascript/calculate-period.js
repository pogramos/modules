function CalculatePeriod(weekEnd, floor) {
	"use strict";

	var self = this;

	// determine if it will ignore weekends or not
	// default[value=true]
	var weeknd = weekEnd || true;

	// determine if it will round minutes or not
	// default[value=true]
	var round = floor || true;

	// Convert ms to actual time (day, hour, min, sec)
	this.msToTime = function(ms) {
		var d, h, hh, m, s;
		// Get the division of 1.000 for seconds
	    s = Math.floor(ms / 1000);
	    // Get the division of 60 for minutes (from seconds)
	    // And correct the amount of seconds
	    m = Math.floor(s / 60);
	    s = s % 60;
	    // Get the division of 60 for hours (from minutes)
	    // And correct the amount of minutes
	    hh = Math.floor(m / 60);
	    m = m % 60;
	    // Get the division of 24 for days (from hours)
	    // And correct the amount of hours
	    d = Math.floor(hh / 24);
	    h = hh % 24;
	    return { days: d, noFormat: hh, hours: h, minutes: m, seconds: s };
	};

	// Sum times to return a new margin object
	this.sumTimes = function(margin, margin2) {
		var d, h, hh, m, s;
	    // sum the seconds,
	    s = margin.seconds + margin2.seconds;
	    // set minutes and correct seconds
	    m = margin.minutes + margin2.minutes + Math.floor(s / 60);
	    s = s % 60;
	    // set hours and correct minutes
	    h = margin.hours + margin2.hours + Math.floor(m / 60);
	    hh = margin.noFormat + margin2.noFormat + Math.floor(m / 60);
	    m = m % 60;
	    // set days and correct hours
	    d = margin.days + margin2.days + Math.floor(h / 24);
	    h = h % 24;

	    return { days: d, noFormat: hh, hours: h, minutes: m, seconds: s };
	};

	// Check a period of time between an interval
	this.checkPeriods = function(margin, startperiod, endperiod) {
		if((weeknd && startperiod.getDay() !== 6 && startperiod.getDay() !== 0) || !weeknd) {
			if(margin.dateStart < startperiod) {

				// if end date is lower than our starting period
				if(margin.dateEnd < startperiod) {
					margin.period = self.sumTimes(margin.period, self.msToTime(startperiod.getTime() - startperiod.getTime()));
				} 
				// if end greater than the starting period but lower than the first period's end
				else if(margin.dateEnd < endperiod) {
					margin.period = self.sumTimes(margin.period, self.msToTime(margin.dateEnd.getTime() - startperiod.getTime()));
				}
				// end outside of the first period
				else {
					margin.period = self.sumTimes(margin.period, self.msToTime(endperiod.getTime() - startperiod.getTime()));
				}
			}
			// if our start date is within our first period
			else if(margin.dateStart < endperiod) {
				// if end is greater than the starting period and lower than the first period's end
				if(margin.dateEnd < endperiod) {
					margin.period = self.sumTimes(margin.period, self.msToTime(margin.dateEnd.getTime() - margin.dateStart.getTime()));
				}
				else {
					margin.period = self.sumTimes(margin.period, self.msToTime(endperiod.getTime() - margin.dateStart.getTime()));
				}
			}

		}
	};

	// Calculate the period between the defined intervals
	this.calculatePeriod = function (start, end, firstPeriodStart, firstPeriodEnd, secondPeriodStart, secondPeriodEnd) {

		if(end < start) { return { days: 0, hours: 0, noFormat:0, minutes: 0, seconds: 0 }; }

		var margin = {
				dateStart: start,
				dateEnd: end,
				period: { days: 0, hours: 0, noFormat:0, minutes: 0, seconds: 0 },
				difference:  { },
				isCurrentOrNextYear: function (a, b) { 
					return a.getFullYear() >= b.getFullYear();
				},
				isNextMonth: function (a, b) {
					// year 2015 and year 2014, month 1 and 12 - true
					return this.isCurrentOrNextYear(a, b) || a.getMonth() >= b.getMonth();
				},
				isNextDay: function () {
					var ending = new Date(this.dateEnd);
					ending.setHours(ending.getHours() + this.difference.hours);
					return (this.isNextMonth(ending, this.dateStart) || ending.getDate() >= this.dateStart.getDate());
				}
			},
			firstPeriod = {
				start: firstPeriodStart,
				end: firstPeriodEnd,
				date: function () {
					var self = this,
						dateStart, 
						dateEnd;
					return {
						start: function (day) {
							dateStart = new Date(margin.dateStart);
							// check for minutes
							var floatingPeriod = (self.start - Math.floor(self.start)) > 0 ? (Math.floor(self.start) * 10) : 0;

							if(typeof day !== "undefined") {
								dateStart.setDate(dateStart.getDate() + day);
							}

							dateStart.setHours(self.start);
							dateStart.setMinutes(floatingPeriod);
							dateStart.setSeconds(0);
							return dateStart;
						},
						end: function (day) {
							dateEnd = new Date(margin.dateStart);
							// check for minutes
							var floatingPeriod = (self.end - Math.floor(self.end)) > 0 ? (Math.floor(self.end) * 10) : 0;

							if(typeof day !== "undefined") {
								dateEnd.setDate(dateEnd.getDate() + day);
							}
							dateEnd.setHours(self.end);
							dateEnd.setMinutes(floatingPeriod);
							dateEnd.setSeconds(0);
							return dateEnd;
						}
					};
				}
			},
			secondPeriod = {
				start: secondPeriodStart,
				end: secondPeriodEnd,
				date: function () {
					var self = this,
						dateStart, 
						dateEnd;
					return {
						start: function (day) {
							dateStart = new Date(margin.dateStart);
							// check for minutes
							var floatingPeriod = (self.start - Math.floor(self.start)) > 0 ? (Math.floor(self.start) * 10) : 0;

							if(typeof day !== "undefined") {
								dateStart.setDate(dateStart.getDate() + day);
							}
							dateStart.setHours(self.start);
							dateStart.setMinutes(floatingPeriod);
							dateStart.setSeconds(0);
							return dateStart;
						},
						end: function (day) {
							dateEnd = new Date(margin.dateStart);
							// check for minutes
							var floatingPeriod = (self.end - Math.floor(self.end)) > 0 ? (Math.floor(self.end) * 10) : 0;

							if(typeof day !== "undefined") {
								dateEnd.setDate(dateEnd.getDate() + day);
							}
							dateEnd.setHours(self.end);
							dateEnd.setMinutes(floatingPeriod);
							dateEnd.setSeconds(0);
							return dateEnd;
						}
					};
				}
			};
		// get the difference between the start and final date
		margin.difference = self.msToTime(margin.dateEnd.getTime() - margin.dateStart.getTime());

		// calculate period based in days
		for(var i = 0; i <= margin.difference.days; i++) {
			//starting period
			var startingPeriod = firstPeriod.date().start(i),
				endingPeriod = firstPeriod.date().end(i),
				starting2ndPeriod = secondPeriod.date().start(i),
				ending2ndPeriod = secondPeriod.date().end(i);

			// lots of date check here
			// if the starting date is lower than the starting period
			self.checkPeriods(margin, startingPeriod, endingPeriod);
			self.checkPeriods(margin, starting2ndPeriod, ending2ndPeriod);

			// if our check is at the end but the difference between hours still put another
			// day at our margin, calculate for the next periods
			if((margin.difference.days - i) === 0 && margin.isNextDay()) {
				var incrementalDay = i + 1;
				startingPeriod = firstPeriod.date().start(incrementalDay);
				endingPeriod = firstPeriod.date().end(incrementalDay);
				starting2ndPeriod = secondPeriod.date().start(incrementalDay);
				ending2ndPeriod = secondPeriod.date().end(incrementalDay);

				self.checkPeriods(margin, startingPeriod, endingPeriod);
				self.checkPeriods(margin, starting2ndPeriod, ending2ndPeriod);
			}
		}

		if(round) {
			// round dates
			if(Math.floor(margin.period.seconds / 30) >= 1) {
				margin.period.minutes++;
				margin.period.seconds = 0;
			}
		}

		return margin.period;
	};
}